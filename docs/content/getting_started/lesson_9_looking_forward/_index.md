---
bookCollapseSection: false
weight: 110
---

# Looking Forward

Thanks for completing this tutorial! I really hope you enjoyed it. If you are interested in purchasing GitLab and working with our sales team, you can [chat with us!](https://about.gitlab.com/sales/)

You can also see other tutorials around security within the [GitLab Developer Evangelism and Technical Marketing Security and Governance](https://gitlab.com/gitlab-de/tutorials/security-and-governance) group.

For more info on our Security and Compliance features, you can visit the solutions pages for each:

- [DevSecOps Solution Page](https://about.gitlab.com/solutions/dev-sec-ops/)
- [Compliance Solution Page](https://about.gitlab.com/solutions/compliance/)

## GitLab Security Direction and New Features

You can see the direction GitLab is taking by checking out the [Secure Direction](https://about.gitlab.com/direction/secure/) as well as the [Govern Direction](https://about.gitlab.com/direction/govern/).

**Note**: The above pages may contain information related to upcoming products, features and functionality. It is important to note that the information presented is for informational purposes only, so please do not rely on the information for purchasing or planning purposes. Just like with all projects, the items mentioned on the pages are subject to change or delay, and the development, release, and timing of any products, features or functionality remain at the sole discretion of GitLab Inc.

## Connecting on Social

You can reach GitLab and connect on a variety of social platforms!

- Twitter: https://twitter.com/gitlab
- LinkedIn: https://www.linkedin.com/company/gitlab-com/
- Facebook: https://www.facebook.com/gitlab
- Instagram: https://www.instagram.com/gitlab/
- Giphy: https://giphy.com/gitlab

## See us at a Conference

You can see what conferences we will be at by visiting the [GitLab Events](https://about.gitlab.com/events/) page. If you are there come by and say hi!!

{{< button relref="/lesson_8_policy_as_code_gitops" >}}Previous Lesson{{< /button >}}
{{< button relref="/" >}}Go Home{{< /button >}}