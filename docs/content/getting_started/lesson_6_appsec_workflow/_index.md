---
bookCollapseSection: false
weight: 80
---

# AppSec Workflows

GitLab offers several Dashboards and Management tools in order to triage and track vulnerabilities. In this lesson, we will go over the Security Dashboard and Vulnerability Management Console.

## Step 1: Viewing Vulnerability Reports / Pages

Each vulnerability report contains vulnerabilities from the scans of the most recent branch merged into the **default** branch. The vulnerability reports display the total number of vulnerabilities by severity, and when clicking on a vulnerability you are provided with the following data:

- date
- status
- severity
- description
- identifier
- the scanner where it was detected
- activity (including related issues or available solutions)
- explanation and mitigation with ai

{{< hint info >}}
**Note:** AI explanation and mitigation is experimental. You can provide feedback in [this issue](https://gitlab.com/gitlab-org/gitlab/-/issues/407295)
{{< /hint >}}

1. Navigate to **Secure** left navigation menu and select **Vulnerability report** 

2. Click on the any vulnerability within the list

3. Select the **Status** box

4. Select **Confirm**

5. Press the **Change Status** button. This allows for better filtering, enabling the security team to better triage security issues

6. Now scroll to the bottom of the page, and add a comment in the text box and press the **Save comment** button. This will save the comment in the Vulnerability page to enable collaboration between different members of the AppSec team

7. Now click on **Create Issue** button. This will take you to an issue creation prompt which allows you to create an issue (confidential or not) in order to collaborate with developers on a resolution.

8. Fill anything you want, scroll down, and click on the **Create Issue** button. Now you have an issue which can be used for AppSec and Development teams to collaborate on resolving the vulnerability.

### Using Explain this vulnerability AI feature

If you want to see the ["explain this vulnerability"](https://about.gitlab.com/blog/2023/05/02/explain-this-vulnerability/) feature, make sure to have enabled the [Group Experiment features setting](https://docs.gitlab.com/ee/user/group/manage.html#group-experiment-features-setting) as seen in [lesson 4](../lesson_4_setting_up_and_configuring_the_security_scanners_and_policies/). Then you can do the following:

1. Navigate to **Secure** left navigation menu and select **Vulnerability report**

2. Sort by the following criteria:

- **Status**: Needs triage
- **Severity**: Medium
- **Tool**: SAST
- **Activity**: Still detected

3. Search for or scroll down to a vulnerability titled **CWE-89 in db.py**

4. Click on the **CWE-89 in db.py** vulnerability

5. Scroll to the **Explain this vulnerability and how to mitigate it with AI** section

6. Click the **Try it out** button

{{< hint info >}}
**Note:** This feature is [Experimental](https://docs.gitlab.com/ee/policy/alpha-beta-support.html#experiment). It may take some time for the response to be generated. If it hangs try reloading the page or trying again at a later time.

Additionally you can share feedback in this [issue](https://gitlab.com/gitlab-org/gitlab/-/issues/407295).
{{< /hint >}}

Viola, you should now see an explanation provided using AI as seen below:

![](/tutorials/security-and-governance/devsecops/simply-vulnerable-notes/images/ai_explain_this_vulnerability.png)

## Step 2: Viewing Audit Events

Audit Events track important events, including who performed the related action and when. You can use audit events to track, for example:

* Who changed the permission level of a particular user for a GitLab project, and when.
* Who added a new user or removed a user, and when.

You can see a list of available Audit Events in the [documentation](https://docs.gitlab.com/ee/administration/audit_events.html).

1. Navigate to **Secure** left navigation menu and select **Audit events**

2. Look over the logged **events**

{{< hint info >}}
**Note:** You can also stream these Audit Events to another tool or endpoint to process them. You can write functions to raise alerts on certain events and much more. See the [Audit Event Streaming documentation](https://docs.gitlab.com/ee/administration/audit_event_streaming.html) for more info.
{{< /hint >}}

## Step 3: Accessing the Security Dashboard

At the project level, the Security Dashboard displays a chart with the number of vulnerabilities introduced to the default branch over time. 

1. Access the Security Dashboard by going to **Secure** left navigation menu and selecting **Security Dashboard**  

**Note:** Nothing will be present, wait a day for it to be populated. Eventually over time with new commits introducing and resolving vulnerabilities, you'll have something like this:  

![](/tutorials/security-and-governance/devsecops/simply-vulnerable-notes/images/security_dashboard.png)

## Step 4: Viewing the Software Bill of Materials

GitLab generates a Software Bill of Materials (SBOM) based on the latest successful scan on the default branch. An [SBOM](https://www.cisa.gov/sbom) is a nested inventory, a list of ingredients that make up software components.

In GitLab we refer to an SBOM as a [Dependency List](https://docs.gitlab.com/ee/user/application_security/dependency_list/). The dependency list shows the path between a dependency and a top-level dependency it’s connected to, if any. There are many possible paths connecting a transient dependency to top-level dependencies, but the user interface shows only one of the shortest paths. It requires:

- The Dependency Scanning or Container Scanning CI job must be configured for your project.
- Your project uses at least one of the languages and package managers supported by Gemnasium.
- A successful pipeline was run on the default branch

1. Navigate to **Secure** left navigation menu and select **Dependency List** 

2. Examine the dependencies present, and look over the provided information:

- **Component**: The dependency’s name and version.
- **Packager**: The packager used to install the dependency.
- **Location**: For system dependencies, this lists the image that was scanned. For application dependencies, this shows a link to the packager-specific lock file in your project that declared the dependency.
- **License**: Links to dependency’s software licenses.

{{< hint info >}}
You can also download the SBOM in [CycloneDX](https://cyclonedx.org/) format by looking at the artifacts generated within the Dependency Scanning Job. For more information on this, see the [Dependency Scanning CycloneDX documentation](https://docs.gitlab.com/ee/user/application_security/dependency_scanning/#cyclonedx-software-bill-of-materials)
{{< /hint >}}

## Step 5: Operational Container Scanning (Optional)

You can use operational container scanning to scan container images in your cluster for security vulnerabilities. You can enable the scanner to run on a cadence as configured via the agent, or setup scan execution policies within a project that houses the agent.

1. Go to the the **Secure** left navigation menu and press **Policies**

2. Click on the **New policy** button   

3. Press the **Select policy** button under the **Scan execution policy** section

4. Fill out the following information:

- Name: Policy Name
- Description: Policy Description

5. Check the **Enabled** button under **Policy status**

6. Under the **Rules** section create a rule with the following specifications:

> Run a `Container Scanning` scan on runner that `selected automatically`

7. Under the **Conditions** section create a condition with the following criteria

> IF `Schedule` actions for the `agent` **simplenotes** in namespaces
  **default, notes-app**
  `daily` at `00:00`

{{< hint info >}}
**Note:**
Make sure that the **agent-name (simplenotes)** is typed in correctly, this will be the agent running in our cluster which we will use to scan our pods for vulnerabilities.

The **namespaces** we are scanning here are **default** and **notes-app**, where all our pods are loaded.

The **00:00** is measured in [UTC](https://www.timeanddate.com/worldclock/timezone/utc) time according to the system-time of the **kubernetes agent (simplenotes)** pod.
{{< /hint >}}

8. Click on the **Configure with a merge request** button, you will be transported to a merge-request

{{< hint info >}}
**Note:** Notice that when creating a merge-request, the new policy is appended to the `policy.yaml` in the new project `<your-project-name>-security-policy-project`. This project was created with the first policy added.
{{< /hint >}}

9. Press the **Merge** button to enable the policy

{{< hint info >}}
**Note:** Now that the policy has been created, we must wait until the scheduled time for the scanner to run. Results may not populate until tomorrow
{{< /hint >}}

### Viewing the Results

1. Go to the **Secure** left navigation menu and press **Vulnerability report**

2. Click on the **Operational vulnerabilities** tab

3. View all the different vulnerabilities found in the cluster

---

Congratulations, you are now able to use some of GitLab's Security Tools within your AppSec Workflow and to better collaborate with others! **This concludes the main tutorial, the next lessons are optional, but will show more GitLab security features**

The next lesson goes over additional scanner configurations and branch protections.

{{< button relref="/lesson_5_developer_workflow" >}}Previous Lesson{{< /button >}}
{{< button relref="/lesson_7_branch_protection_and_additional_scanner_configuration" >}}Next Lesson{{< /button >}}